set guioptions-=T "remove toolbar

colorscheme molokai
"set background=dark

set guifont=Droid\ Sans\ Mono\ 10 

" Highlight EOL whitespace
" (http://sartak.org/2011/03/end-of-line-whitespace-in-vim.html)
" For some reason this has to be repeated in .gvimrc
autocmd InsertEnter * syn clear EOLWS | syn match EOLWS excludenl /\s\+\%#\@!$/
autocmd InsertLeave * syn clear EOLWS | syn match EOLWS excludenl /\s\+$/
highlight EOLWS ctermbg=red guibg=red

