set nocompatible

"" Vundle setup and init.
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

"" Handle itself, required
Plugin 'gmarik/Vundle.vim'

"" Start plugins
Plugin 'tomasr/molokai'
Plugin 'altercation/vim-colors-solarized'
Plugin 'sickill/vim-monokai'

Plugin 'nathanaelkane/vim-indent-guides'
"" indentLine has characters such as | as guides, so it's more discreete.
"Plugin 'Yggdroot/indentLine'

Plugin 'tpope/vim-fugitive'

Plugin 'Raimondi/delimitMate'
"" Auto Pairs seems good too.  TODO: try it out.
"Plugin 'jiangmiao/auto-pairs'

Plugin 'mattn/emmet-vim'

Plugin 'itchyny/lightline.vim'
"Plugin 'bling/vim-airline'
"" End plugins

call vundle#end()
filetype plugin indent on

"" End Vundle setup.

"" My settings.

"set nomodeline
set ruler
set number
syntax on

autocmd FileType text setlocal textwidth=78

"" Highlight EOL whitespace
"" (http://sartak.org/2011/03/end-of-line-whitespace-in-vim.html)
autocmd InsertEnter * syn clear EOLWS | syn match EOLWS excludenl /\s\+\%#\@!$/
autocmd InsertLeave * syn clear EOLWS | syn match EOLWS excludenl /\s\+$/
highlight EOLWS ctermbg=red guibg=red

"" This visualizes tabs, it's here to remind me how to do it b/c I don't want
"" it in every file
"set list listchars=tab:>-

"" Mark 80th column.
set colorcolumn=80

"" Indent Guides settings.
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1

"" delimitMate settings.
let g:delimitMate_nesting_quotes = ['"', "'"]
let g:delimitMate_expand_cr = 2
let g:delimitMate_expand_space = 1
let g:delimitMate_expand_inside_quotes = 1

"" Lightline settings.
set laststatus=2
set noshowmode

